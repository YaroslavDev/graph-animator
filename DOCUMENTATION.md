### Graph Animator - Programmer's documentation

#### Project structure  
Project consists of 2 classes and 1 file: __GraphAnimConfig.py__, __GraphAnim.py__ and __main.py__.

#### GraphAnimConfig class
Represents common configuration object that is used by GraphAnim instance. We have 3 sources of parameters: configuration file, command line and default values. It is useful to have single structure that stores all configuration and not to work separately with config file and commandline arguments. GraphAnimConfig class is responsible for merging specified parameters in single structure. 

Constructor takes object that stores parsed arguments from __argparse__ module.

Parameters are set in the following order:

	* Default values  
	* Values from configuration file  
	* Values from command line  

Values from current step can overwrite values from previous.

After parsing and setting is done, this structure can be used for GraphAnim instance.

#### GraphAnim class
Constructor takes as parameter GraphAnimConfig instance, loads data from sources, computes auxiliary parameters such as Time, Speed, FPS, Min-Max X-Y.

Method __renderFrames()__ calls __setup__ that creates gnuplot subprocess, sets up axes and returns reference to gnuplot subprocess. Then __renderFrames()__ creates output directory and in for loop iterates from loaded data rendering each frame in one file.

#### Main.py

In this file instance of ArgumentParser is created, all necessary parameters are registered and parsed. This instance is used in creation of GraphAnimConfig object which parses config file(if specified). Configuration object is passed to GraphAnim object on which method __renderFrames()__ is called.