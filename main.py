from argparse import ArgumentParser

from GraphAnimConfig import GraphAnimConfig
from GraphAnim import GraphAnim

# Set ArgumentParser() object
parser = ArgumentParser()

parser.add_argument('-t')                           # time format
parser.add_argument('-X')                           # x max value
parser.add_argument('-x')                           # x min value
parser.add_argument('-Y')                           # y max value
parser.add_argument('-y')                           # y min value
parser.add_argument('-S', type=float)               # speed in record / frame
parser.add_argument('-T', type=float)               # duration of animation in seconds
parser.add_argument('-F', type=float)               # frames per second
parser.add_argument('-c', action='append')          # critical values* int/float
parser.add_argument('-l')                           # legend text
parser.add_argument('-g', action='append')          # Gnuplot parameters
parser.add_argument('-e', action='append')          # Effect parameters
parser.add_argument('-f')                           # Configuration file
parser.add_argument('-n')                           # Frames directory name
parser.add_argument('-E', action='store_true', default=False)      # Ignore errors, just print warning messages
parser.add_argument('sources', nargs='+')

# Parse known arguments
args, unknown = parser.parse_known_args()

# Check if there are unknown
if (not args.E and unknown) :
    raise ValueError("Unrecognized command line parameter(s): {0}!".format(unknown))

# Create configuration object
cfg = GraphAnimConfig(args)

# Create GraphAnimation object and make animation
animator = GraphAnim(cfg)
animator.renderFrames()
#animator.makeVideo()
