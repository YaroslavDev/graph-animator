## Graph Animator

### Description
Python script that makes a .gif animation file from source(s) of data: file or URL. Source contains lines with time value and float value(e.g. [2009/05/11 13:27:00] -0.4154). 

### Usage
__usage:__ python3 main.py [-h] [-t T] [-X X] [-x X] [-Y Y] [-y Y] [-S S] [-T T] [-F F]
               [-c C] [-l L] [-g G] [-e E] [-f F] [-n N] [-E]
               sources [sources ...]  
__example:__  
python main.py -t %y/%m/%d -X 09/12/30 -x 09/01/01 -Y 1000 -y -1000 -S 5 -F 15 -c x=09/04/01 -c x=09/09/01 -c y=500:y=590:y=600:x=09/07/01 -l "Example animation - Simple effect" -g "grid xtics ytics" -g "pointsize 10" -g "tics textcolor rgbcolor \"blue\"" -e bgcolor=red:width=32:shadow=none -n test_animation -E input_file_1 input_file_2 input_file_3
               
### Parameter description    
|opt.|directive|description| value type| default value |
|---|-----------|----------|-----------|---------------|
| -t| TimeFormat| timestamp| format strftime(3c)| [%Y-%m-%d %H:%M:%S] |
|-X	| Xmax	| x-max	|“auto”,”max”,value | max |
|-x	|Xmin	|x-min	|“auto”,”min”,value	 |min |
|-Y	|Ymax	|y-max	|“auto”,”max”,value	 |auto | 
|-y	|Ymin	|y-min	|“auto”,”min”,value	 |auto |
|-S	|Speed	|speed	|int/float	 |1 record/frame |
|-T	|Time	|time (duration) |int/float	 | n/a |
|-F	|FPS	|fps	|int/float	|25 |
|-c	| CriticalValue	 | critical values*	 |[xy]=int/float |n/a |
|-l	 |Legend |legend |text	|n/a |
|-g	 |GnuplotParams	|gnuplot params* | parameter |n/a |
|-e	 |EffectParams	| effect| params* param=val:param=val |n/a |
|-f	 |n/a	|config |file	 |pathname	 |n/a |
|-n	 |Name	| name	 |text	 |n/a |
|-E	 |IgnoreErrors	 |don't ignore errors |n/a, true/false |true |

### Configuration file description:

* Lines ends with UNIX EOL character (x10, \n).  
* Text starting with # is a comment that extends to the end of the line.  
* Empty lines are ignored as well as lines containing only blank characters (spaces, tabs).
* One line can contain one directive at most.
* Any directive consists of exactly one word.
* Config file disregards character capitalisation (is case-insensitive).
* A directive has just one value (is equivalent to one command line argument).
* A directive can be stated repeatedly in the configuration file, the number of occurrences is the same as is the limit of occurrences of equivalent command line arguments (* character in the table of arguments means any number of occurrences, otherwise at most one occurrence is allowed).
* If the directive can be used only once, only the last occurrence of the directive will be applied.
* Directive is separated from its value by a space or a tab (or by a combination of them).


