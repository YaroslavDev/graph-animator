class GraphAnimConfig:
    """
    Instance of this class is filled with configuration necessary for animation and
    passed as argument to GraphAnim class constructor.
    """

    def __init__(self, args=None):
        """
        Constructor initializes properties with default values,
        first loads configuration from configuration file(if provided) and then from command line.
        Command line arguments take precedence so values from cmd can overwrite values from config file.
        """
        self.time_fmt = "[%Y-%m-%d %H:%M:%S]"   # time format of input data
        self.x_max = "max"              # greatest value of x
        self.x_min = "min"              # smallest value of x
        self.y_max = "auto"             # greatest value of y
        self.y_min = "auto"             # smallest value of y
        self.speed = 1.0                # record / frame
        self.speed_specified = False    # if speed was specified by user
        self.time = None                # duration
        self.time_specified = False     # if time was specified by user
        self.fps = 25                   # frames / second
        self.fps_specified = False      # if fps was specified by user
        self.crit_vals = []             # int / float
        self.legend = None              # legend text
        self.gnuplot_params = []        # parameters for gnuplot subprocess
        self.effect_params = []         # animation params
        self.cfg_file = None            # configuration file
        self.dir_name = "GraphAnim"     # frames directory name
        self.ignore_errors = False      # ignore errors(y/n)
        self.sources = []

        if (args):
            if (args.f):
                self._setConfigureFile_(args.f)
                self.loadFromFile(self.cfg_file)
            self.loadFromArgs(args)


    def loadFromFile(self, cfg_filename):
        """
        Loads configuration from specified configuration file.
        """
        COMMENT_CHAR = '#'
        try:
            with open(cfg_filename) as f:
                for line in f:
                    line = line.rstrip()
                    if COMMENT_CHAR in line:
                        line, comment = line.split(COMMENT_CHAR, 1)
                    if line and not line.isspace():
                        directive, value = line.split(maxsplit=1)
                        try:
                            {
                                "timeformat" : self._setTimeFormat_,
                                "xmax" : self._setXMax_,
                                "xmin" : self._setXMin_,
                                "ymax" : self._setYMax_,
                                "ymin" : self._setYMin_,
                                "speed" : self._setSpeed_,
                                "time" : self._setTime_,
                                "fps" : self._setFPS_,
                                "criticalvalue" : self._setCriticalValues_,
                                "legend" : self._setLegend_,
                                "gnuplotparams" : self._setGnuplotParams_,
                                "effectparams" : self._setEffectParams_,
                                "name" : self._setDirectoryName_,
                                "ignoreerrors" : self._setIgnoreErrors_
                            }[directive.lower()](value)
                        except KeyError:
                            warning = "Unknown directive {0} in configuration file".format(directive)
                            if (not self.ignore_errors):
                                raise ValueError(warning)
                            else:
                                print(warning)
        except FileNotFoundError:
            warning = "Configuration file {0} not found!".format(cfg_filename)
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)


    def loadFromArgs(self, args):
        """
        Loads configuration from parsed by argparse module command line arguments.
        """
        if args.E:
            self._setIgnoreErrors_(args.E)
        if args.t:
            self._setTimeFormat_(args.t)
        if args.X:
            self._setXMax_(args.X)
        if args.x:
            self._setXMin_(args.x)
        if args.Y:
            self._setYMax_(args.Y)
        if args.y:
            self._setYMin_(args.y)
        if args.S:
            self._setSpeed_(args.S)
        if args.T:
            self._setTime_(args.T)
        if args.F:
            self._setFPS_(args.F)
        if args.c:
            self._setCriticalValues_(args.c)
        if args.l:
            self._setLegend_(args.l)
        if args.g:
            self._setGnuplotParams_(args.g)
        if args.e:
            self._setEffectParams_(args.e)
        if args.f:
            self._setConfigureFile_(args.f)
        if args.n:
            self._setDirectoryName_(args.n)
        self.sources = args.sources

    # Below there are setters for parameters
    def _setTimeFormat_(self, time_fmt):
        if (type(time_fmt) is str):
            time_fmt = time_fmt.lstrip().rstrip()
            self.time_fmt = time_fmt
        else:
            warning = "TIME_FMT should be string"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setXMax_(self, x_max):
        if (type(x_max) is str):
            x_max = x_max.lstrip().rstrip()
        self.x_max = x_max

    def _setXMin_(self, x_min):
        if (type(x_min) is str):
            x_min = x_min.lstrip().rstrip()
        self.x_min = x_min

    def _setYMax_(self, y_max):
        if (type(y_max) is str):
            y_max = y_max.lstrip().rstrip()
        self.y_max = y_max

    def _setYMin_(self, y_min):
        if (type(y_min) is str):
            y_min = y_min.lstrip().rstrip()
        self.y_min = y_min

    def _setSpeed_(self, speed):
        try:
            self.speed = float(speed)
            self.speed_specified = True
        except ValueError:
            warning = "SPEED should be int or float"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setTime_(self, time):
        try:
            self.time = float(time)
            self.time_specified = True
        except ValueError:
            warning = "TIME should be int or float"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setFPS_(self, fps):
        try:
            self.fps = float(fps)
            self.fps_specified = True
        except ValueError:
            warning = "FPS should be int or float"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setCriticalValues_(self, crit_vals):
        if (type(crit_vals) is str):
            crit_vals = crit_vals.rstrip().lstrip()
            split = crit_vals.split('=')
            len_split = len(split)
            fmtstr = crit_vals[:2]
            for i in range(1, len_split - 1):
                len_part = len(split[i])
                split[i] = split[i][:len_part - 2] + ',' + split[i][len_part - 1:] + '='
                fmtstr += split[i]
            fmtstr += split[len_split - 1]
            self.crit_vals += fmtstr.split(',')
        elif (type(crit_vals) is list):
            for crit_val in crit_vals:
                self._setCriticalValues_(crit_val)
        else:
            warning = "CRIT_VALS should be list or string"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setLegend_(self, legend):
        if (type(legend) is str):
            self.legend = legend
        else:
            warning = "LEGEND should be str"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setGnuplotParams_(self, params):
        if (type(params) is str):
            params = params.rstrip().lstrip()
            self.gnuplot_params.append(params)
        elif (type(params) is list):
            self.gnuplot_params += params
        else:
            warning = "PARAMS should be list or string"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setEffectParams_(self, params):
        if (type(params) is str):
            params = params.rstrip().lstrip()
            self.effect_params += params.split(':')
        elif (type(params) is list):
            for param in params:
                self._setEffectParams_(param)
        else:
            warning = "PARAMS should be list or string"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setConfigureFile_(self, cfg_file):
        if (type(cfg_file) is str):
            self.cfg_file = cfg_file
        else:
            warning = "CFG_FILE should be string"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setDirectoryName_(self, dir_name):
        if (type(dir_name) is str):
            self.dir_name = dir_name
        else:
            warning = "DIR_NAME should be string"
            if (not self.ignore_errors):
                raise ValueError(warning)
            else:
                print(warning)

    def _setIgnoreErrors_(self, ignore_errors):
        if (type(ignore_errors) is str):
            ignore_errors = (True if ignore_errors.lower() == 'true' else False)
        elif (not type(ignore_errors) is bool):
            raise ValueError("IGNORE_ERRORS flag should be of type BOOL")
        self.ignore_errors = ignore_errors
