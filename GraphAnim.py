from urllib import request
import subprocess
import os
import re

class GraphAnim:
    """
    This class having configuration object can render data in .gif animation file
    using renderFrames() method.
    """

    def __init__(self, cfg):
        """
        Constructor creates some additional parameters that are necessary for computing animation
        parameters, setting rendering options and structure that stores loaded data from sources.
        """
        self.data = []
        self.cfg = None
        self.speed = None
        self.time = None
        self.fps = None
        self.dir_name = None
        self.canvas_width = 640
        self.canvas_height = 480
        self.line_width = 2
        self.setConfiguration(cfg)

    def setConfiguration(self, cfg):
        """
        Method stores reference to configuration object, loads data from specified sources(files of URLs),
        computes min and max values of X and Y axes and computes TimeSpeedFPS triplet from user-specified params.
        """
        self.data = []
        self.cfg = cfg
        for source in self.cfg.sources:
            if (self._isUrl_(source)):
                self._loadUrl_(source)
            else:
                self._loadFile_(source)
        self.min_x, self.max_x, self.min_y, self.max_y = (None, None, None, None)
        self._computeMinMaxXY_()
        self._computeTimeSpeedFPS_()

    def _setupAxes_(self, plot):
        """
        Called before actual rendering.
        Sets min and max values of X and Y axes.
        """
        xfmt = "x\"%H:%M\""
        plot.stdin.write("set xdata time\n")
        plot.stdin.write("set format {0}\n".format(xfmt))
        x_range = "["
        if (self.cfg.x_min == "min"):
            x_range += "\"" + str(self.min_x) + "\""
        elif (self.cfg.x_min != "auto"):
            self.min_x = self.cfg.x_min
            x_range += "\"" + str(self.cfg.x_min) + "\""
        x_range += ":"
        if (self.cfg.x_max == "max"):
            x_range += "\"" + str(self.max_x) + "\""
        elif (self.cfg.x_max != "auto"):
            self.max_x = self.cfg.x_max
            x_range += "\"" + str(self.cfg.x_max) + "\""
        x_range += "]"
        if (x_range != "[:]"):
            plot.stdin.write("set xrange {0}\n".format(x_range))
        # Set y-range
        y_range = "["
        if (self.cfg.y_min == "min"):
            y_range += "\"" + str(self.min_y) + "\""
        elif (self.cfg.y_min != "auto"):
            self.min_y = self.cfg.y_min
            y_range += "\"" + str(self.cfg.y_min) + "\""
        y_range += ":"
        if (self.cfg.y_max == "max"):
            y_range += "\"" + str(self.max_y) + "\""
        elif (self.cfg.y_max != "auto"):
            self.max_y = self.cfg.y_max
            y_range += "\"" + str(self.cfg.y_max) + "\""
        y_range += "]"
        if (y_range != "[:]"):
            plot.stdin.write("set yrange {0}\n".format(y_range))

    def _computeTimeSpeedFPS_(self):
        """
        User can specify 0, 1 or 2 values from TimeSpeedFPS triplet. Rest of values are computed here.
        If user specifies all 3 values, then method checks if they are consistent.
        """
        num_records = len(self.data)
        self.speed = None
        self.time = None
        self.fps = None
        if (self.cfg.time_specified):
            self.time = self.cfg.time
        elif (self.cfg.speed_specified):
            self.speed = self.cfg.speed
        elif (self.cfg.fps_specified):
            self.fps = self.cfg.fps
        if (not self.time):
            if (not self.speed):
                self.speed = self.cfg.speed  # if was not specified use default value
            if (not self.fps):
                self.fps = self.cfg.fps      # if was not specified use default value
            self.time = num_records / (self.speed * self.fps)
        else:
            if (not self.speed):
                if (not self.fps):
                    self.fps = self.cfg.fps  # if was not specified use default value
                self.speed = num_records / (self.time * self.fps)
            else:
                if (self.fps):
                    if (self.time != (num_records / (self.speed * self.fps))):  # consistency check
                        warning = "Time, Speed and FPS specified - cannot satisfy them all!"
                        if (self.cfg.ignore_errors):
                            raise ValueError(warning)
                        else:
                            print(warning)
                else:
                    self.fps = num_records / (self.time * self.speed)


    def _setup_(self):
        """
        This method creates gnuplot subprocess, sets terminal, time format and user-defined gnuplot params,
        setups axes and returns reference to gnuplot process.
        """
        plot = subprocess.Popen('/usr/local/bin/gnuplot', stdin=subprocess.PIPE, universal_newlines=True)
        # Set terminal type PNG
        plot.stdin.write("set term gif animate delay {0}\n".format(round(100.0 / self.fps)))
        # Set time format
        plot.stdin.write("set timefmt \"{0}\"\n".format(self.cfg.time_fmt))
        # ======================================================
        # Set x-range
        self._setupAxes_(plot)
        # ======================================================
        # Set addition Gnuplot parameters
        for param in self.cfg.gnuplot_params:
            plot.stdin.write("set {0}\n".format(param))
        return plot

    def renderFrames(self):
        """
        Method calls _setup_() method, creates folder for output, renders frames in single .gif animation file
        using gnuplot native functionality.
        """
        plot = self._setup_()
        frame_num = 0
        self.dir_name = self.cfg.dir_name
        if (os.path.exists(self.dir_name)):
            found_dir = False
            for i in range(10):
                next_dir_name = self.dir_name + "_{0}".format(i)
                if (not os.path.exists(next_dir_name)):
                    self.dir_name = next_dir_name
                    found_dir = True
                    break
            if not found_dir:
                raise ValueError("Too many folders with specified name!")
        os.makedirs(self.dir_name)

        frame_range = range(1, len(self.data), round(self.speed))
        if (frame_range[len(frame_range) - 1] != len(self.data) - 1):
            frame_range = list(frame_range)
            frame_range.append(len(self.data) - 1)
        self._setupEffects_(plot)
        plot.stdin.write("set output \"{0}/animation.gif\"\n".format(self.dir_name))
        for i in frame_range:
            #plot.stdin.write("set output    \"{0}/frame_{1}.png\"\n".format(self.dir_name, frame_num))
            for crit_line in self.cfg.crit_vals:
                axis, value = crit_line.split('=')
                if (axis == 'x'):
                    plot.stdin.write("set arrow from \"{0}\",{1} to \"{0}\",{2} nohead front\n".format(value, self.min_y, self.max_y))
                elif (axis == 'y'):
                    plot.stdin.write("set arrow from \"{0}\",{1} to \"{2}\",{1} nohead front\n".format(self.min_x, value, self.max_x))
                else:
                    warning = "Unrecognized critical value!"
                    if (not self.cfg.ignore_errors):
                        raise ValueError(warning)
                    else:
                        print(warning)
            plot.stdin.write("plot '-' using 1:3 with lines linewidth {1} title \"{0}\"\n".format(self.cfg.legend, self.line_width))
            for j in range(i):
                plot.stdin.write("{0}\n".format(self.data[j]))
            plot.stdin.write("e\n")
            frame_num += 1
        plot.stdin.write("set output\n")

    def makeVideo(self):
        """
        Obsolete, not used now.
        """
        ffmpeg_command = ['/usr/local/bin/ffmpeg',
                          '-i', '{0}/frame_%d.png'.format(self.dir_name),
                          '-y',
                          '-r', str(self.fps),
                          '{0}/vidos.mpg'.format(self.dir_name)]
        string_command = " ".join(ffmpeg_command)
        subprocess.call(ffmpeg_command)

    def _loadFile_(self, filename):
        """
        Loads data from file.
        """
        with open(filename) as f:
            for line in f:
                self.data.append(line)

    def _loadUrl_(self, url):
        """
        Loads data from URL.
        """
        with request.urlopen(url) as f:
            for line in f:
                self.data.append(line.decode('utf-8'))

    def _isUrl_(self, source):
        """
        Checks if source string has URL format.
        """
        return re.match("(http|https)://", source)

    def _computeMinMaxXY_(self):
        """
        Computes min and max values of X and Y data.
        """
        minx_split = self.data[0].split()
        minx_split_len = len(minx_split)
        self.min_x = " ".join(minx_split[:minx_split_len - 1])

        maxx_split = self.data[len(self.data) - 1].split()
        maxx_split_len = len(maxx_split)
        self.max_x = " ".join(maxx_split[:maxx_split_len - 1])

        self.min_y = float('inf')
        self.max_y = float('-inf')
        for line in self.data:
            line_split = line.split()
            line_split_len = len(line_split)
            line_num = float(line_split[line_split_len - 1])
            if (line_num < self.min_y):
                self.min_y = line_num
            if (line_num > self.max_y):
                self.max_y = line_num

    def _setupEffects_(self, plot):
        """
        Sets extra effects like background color and width of graph line.
        """
        for effect in self.cfg.effect_params:
            directive, value = effect.split('=')
            try:
                {
                    "bgcolor" : self._setBackgroundColor_,
                    "width" : self._setLineWidth,
                }[directive.lower()](plot, value)
            except KeyError:
                warning = "Unknown effect {0}".format(directive)
                if (not self.cfg.ignore_errors):
                   raise ValueError(warning)
                else:
                    print(warning)

    # Self explanatory methods
    def _setBackgroundColor_(self, plot, value):
        plot.stdin.write("set object 1 rectangle from screen 0,0 to screen 1,1 fillcolor rgb \"{0}\" behind\n".format(value))

    def _setLineWidth(self, plot, value):
        self.line_width = value

    def _setCanvasWidth(self, plot, value):
        self.canvas_width = value
        plot.stdin.write("set terminal png size {0},{1}\n".format(self.canvas_width, self.canvas_height))

    def _setCanvasHeight_(self, plot, value):
        self.canvas_height = value
        plot.stdin.write("set terminal png size {0},{1}\n".format(self.canvas_width, self.canvas_height))